/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heranca.exercicio0;

import java.util.Date;

/**
 *
 * @author gpinho
 */
public class Cliente extends Pessoa {
    
    private Date ultimoAcesso;

    public Cliente(String usuario, String nome, String senha, String cpf) {
        super(usuario, nome, senha, cpf);
    }   

    @Override
    public String toString() {
        return "Cliente{" + super.toString() + " ultimoAcesso=" + ultimoAcesso + '}';
    }
    
    public void acesso(){
        Date data = new Date(System.currentTimeMillis());
        this.ultimoAcesso = data;
    }
    
    
}
