/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heranca.exercicio0;

/**
 *
 * @author gpinho
 */
public class Pessoa {
     
    private String usuario;
    private String nome;
    private String senha;
    private String cpf;
    private String status;

    public Pessoa(String usuario, String nome, String senha, String cpf) {
        this.usuario = usuario;
        this.nome = nome;
        this.senha = senha;
        this.cpf = cpf;
        this.status = "Ativo";
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "usuario=" + usuario + ", nome=" + nome + ", senha=" + senha + ", cpf=" + cpf + ", status=" + status;
    }
    
    
    public void inativar(){
        this.status = "Inativo";
    }
    
}
