/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esquenta.ads;

/**
 *
 * @author rklaes
 */
public class Jogador {
    private String name;
    private double value;
    private String position;

    public Jogador(String name, double value, String position) {
        this.name = name;
        this.value = value;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public String getPosition() {
        return position;
    }
}
