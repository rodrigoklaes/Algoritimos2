/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esquenta.ads;

import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class ObjsJogador {
    
    public static ArrayList<String> getNome() {
        String[] list = {"Angelo", "Maicom", "Luan", "Rodrigo", "Matheus",
            "Christopher", "Élvis", "João", "Paulo", "Arnaldo", "Gilberto", "Rogério", "Mario"};
        return Gerador.incrementaArray(list);
    }

    public static ArrayList<String> getSobrenome() {
        String[] list
                = {"Costa", "Silva", "Souza", "Araujo", "Ferreira", "Flores", "Cunha",
                    "Jesus", "Almeida", "Andrades", "Neto", "Peixoto", "Alves", "Garcia",
                    "Gomez", "Barreto", "Couto", "Junior"};
        return Gerador.incrementaArray(list);
    }

    public static ArrayList<String> getPosicao() {
        String[] list
                = {"Atacante", "Meia", "Volante", "Zagueiro", "Lateral", "Goleiro"};
        return Gerador.incrementaArray(list);
    }
    
    
    
}
