/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esquenta.ads;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author rklaes
 */
public class EsquentaADS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        ArrayList<Jogador> jogadores = Gerador.gerarJogadores(10);
        
        for (Jogador jogador : jogadores) {
            System.out.println("Nome: " + jogador.getName() + " - Valor: " + jogador.getValue() + " - Posição: " + jogador.getPosition());
        }
        
        int valor = Gerador.gerarValor(100000, 1000000);
      
        System.out.println("Valor: " + valor);
        
        /*FileWriter arq = new FileWriter("C:\\Users//rklaes//Documents//NetBeansProjects/dbFile.txt");
        PrintWriter gravaArq = new PrintWriter(arq);
        
        gravaArq.println("---Jogadores---");
        for (Jogador jogador : jogadores) {
            gravaArq.println("Nome: " + jogador.getName() + " - Valor: " + jogador.getValue() + " - Posição: " + jogador.getPosition());
        }*/
        
    }
    
}
