/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esquenta.ads;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
/**
 *
 * @author rklaes
 */
public class Gerador {

    public static ArrayList<String> incrementaArray(String[] itens){
        
        ArrayList<String> list = new ArrayList();
        list.addAll(Arrays.asList(itens));
        
        return list;
    }
    
    public static String gerarAll(Random gerar, ArrayList<String> objs) {        
        ArrayList<String> obj = objs;
        return obj.get(gerar.nextInt(obj.size()));
    }
    
    public static int gerarValor(int valorMaximo, int valorMinimo){
        Random gerar = new Random();
        return gerar.nextInt(valorMaximo) + valorMinimo;
    }

    public static ArrayList<Jogador> gerarJogadores(int qtdaJogador) {
        Random gerar = new Random();

        ArrayList<Jogador> jogadores = new ArrayList();

        for (int i = 0; i < qtdaJogador; i++) {
            Jogador jogador;
            jogador = new Jogador((gerarAll(gerar, ObjsJogador.getNome()) + " "
                    + gerarAll(gerar, ObjsJogador.getSobrenome())), gerarValor(10000, 100000),
                    gerarAll(gerar, ObjsJogador.getPosicao()));

            jogadores.add(jogador);
        }

        return jogadores;
    }
}
